# QA automation assignment

This assignment is meant to evaluate the QA Automation proficiency of full-time engineers.

## Evaluation points in order of importance

- correctly defined test cases
- readiness for CI
- correct error messages
- documentation: README and inline code comments

Results: please share a git repository with us containing your implementation.

Level of experience targeted: Junior

Usage of Cypress and BDD is preferred but not essential.
If you have questions please make some assumptions and collect in writing your interpretations.

Good luck.

## Technical test

Given an application(oos_ca). Run it using the instructions in README.md. Observe the application functionality, assuming that it works correctly.
Create automation framework that will make sure that behavior stays consistent in future versions.  
Create documentation on how to work with your test framework.

## Bonus points

- Use Docker for tests

## Note

**Do not mention 90 percent of everything or 90poe anywhere on the code or repository name.**
